# Development team management application

This application aims to list all the tasks performed by the development team and registered in project control tools.
The main objective is to provide means to supervise the daily production of the developers.

## Use cases

- As a project manager,I want to view the list of projects in development.
  - I need to view the list of projects under development in Bitbucket.
  - I need to view the list of projects under development in Gitlab.
  - I need to view the list of projects under development in Jira.
  - I need to view the list of projects under development in Tiga.
  - I need to view the list of projects under development in new tools to be selected.

- As a project manager, I want to get all commits from a developer to do function point count.
  - ✅ I need to get commits from Bitbucket
  - ✅ I need to get commits from Gitlab
    - condition: Get commits, issues and discussions daily.
  - ✅ I need to get posts from Jira
    - condition: Get stories, epics, tasks, and improvements from Jira daily.
      - subject: get_jira_items function
      - action: scrape jira web application
      - object: get items from all users
      - restriction: a specified period
  - ✅ I need to get posts from Taiga

## Register development team

## Register URLs of project control tools

## Define data structure to be extracted from each URL

## Access each URL and scrape data from it (repeat for all pagination)

### Data

- url

## Save data read in database

## Generate report of tasks performed by developer

## WAF (Web Application Firewall)

- ModSecurity / Cloudflare / Akamai

## API Gateway (Kong Gateway)

### ALB (Application Load Balance)

- Nginx

### IDM (Identity Management)

- Keycloak >> DB

### TLS

- Encrypt

### Secret Manager

- Vault-Hashcorp / ???

## Observability (Elastic Stack)

### Logs (Ingest: filebeat)

- filebeat-7.9.3
  - filebeat.yml
    - hosts: ["localhost:9200"]  # Define Elasticsearch output.
    - cloud.id: "nameDev: dfkdjlkjRRLJLDDSLJLSDKJLDSJFLJF..."
    - cloud.auth: "elastic:**************************"
  - /filebeat modules enable system  # Enable file system.yml
  - ./filebeat setup
  - ./filebeat -e
  - filebeat-7.9.3/modules.d/system.yml
    - var.paths: ["/var/logs/*.log"]
    - type: log
      enable: tue
      paths: # Insert here your app log path.
      - path your application.

### Metrics

- metricbeat-7.9.3
- metricbeat.yml
  - hosts: ["localhost:9200 "]  # Define Elasticsearch output.
    - cloud.id: "nameDev: dfkdjlkjRRLJLDDSLJLSDKJLDSJFLJF..."
    - cloud.auth: "elastic:**************************"
- /metricbeat modules enable system  # Enable file system.yml
- ./metricbeat setup
- ./metricbeat -e
- filebeat-7.9.3/modules.d/system.yml
  - var.paths: ["/var/logs/*.log"]

#### Service Metrics (Receive data from broker / RabbitMQ)

- Prometheus
- Grafana
- Alert Manager

### Tracing

- APM-Application Performance Monitor

## ACL (Anti-corruption layer - Communication through Webhook)

- ALB >> App Microservice >> Gateway/Webhook

## CI

- Pipeline
  - pull request
  - Tests
  - Quality (SonarQube)
  - Code review
  - Marge
  - Packaging

## CD

- Pipeline
  - Tests
  - Approval
  - Deploy (Blue/green - Canary)

## Production

- Kubernetes
- Prometheus/Grafana

## Technology Infrastructure

### Create local Kubernetes cluster

```bash
k3d cluster create -p "8000:30000@loadbalancer" --agents 2
```

### DB (with replica/CQRS - Everything versioned with a timestamp to prevent concurrency - Event Sourcing )

- SQL/RDBMS (Schemas)
- NoSQL (Collections) - document, key-value, wide-column, and graph.
- Redis (in memory)
