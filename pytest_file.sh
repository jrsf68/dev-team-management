clear
date +"%d/%m/%Y %H:%M:%S"
FILE="$1"
echo "File: $FILE"
blue --check --diff $FILE && isort --check --diff $FILE
pytest --ff -x -vv -s --capture=no $FILE
coverage html
date +"%d/%m/%Y %H:%M:%S"
