# Dev Team Management Documentation

## Python development procedures

---

## Roadmap for development

### Create project with poetry

```dash
poetry new project_name
cd project_name
```

### Generate documentation

#### Documentation site with mkdocs

```dash
mkdocs new .   # Create file 'mkdocs.yml' and folder 'docs'
mkdocs serve  # run server
```

### Configuration pytest

[tool.pytest.ini_options]  
pythonpath = "."  
addopts = "--doctest-modules"

```dash
poetry add --group dev pytest
poetry add --group dev pytest-cov
```

### Configuration linter

[tool.isort]  
profile = "black"  
line_length = 79

```dash
poetry add --group dev blue
poetry add --group dev isort
poetry add --group dev taskipy
```

### Configuration doc

<!-- [tool.isort]
profile = "black"
line_length = 79 -->

```dash
poetry add --group doc mkdocs-material
poetry add --group doc mkdocstrings
poetry add --group doc mkdocstrings-python
```

---

## App documentation

### Summary

### Installation

#### How to install

##### Clone

#### How to Run

##### Tests

##### Linter

#### How to use

##### API

### Tutorial

#### Goals

### How to Contribute

### help

#### FAQ

#### Release/Fix
