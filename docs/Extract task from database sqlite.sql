SELECT uuid, tool, developer_full_name, developer_email, developer_name, project, task_id, task, complement, completion_date, status
FROM tasks;
SELECT	UPPER(SUBSTRING(tool,1,1)) ||'|'|| 
		UPPER(developer_full_name) ||'|'||
		UPPER(project) ||'|'||
		task ||'-'||
		tool ||' '||
		CASE WHEN tool = 'gitlab' THEN COALESCE(complement, '') ELSE '' END ||'|'||
		'||'||
		SUBSTRING(completion_date,9,2) ||'/'||
		SUBSTRING(completion_date,6,2) ||'/'||
		SUBSTRING(completion_date,1,4) ||'|'||
		'|'||
		'Concluída' 
FROM tasks; 
