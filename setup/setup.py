"""Functions to configure the application.
"""
from json import load
from os import getenv

from dotenv import load_dotenv

load_dotenv('.env')
load_dotenv('.env.secrets')


def read_setup():
    """Reads json file with app configuration."""
    with open('app/setup/setup.json', 'r', encoding='UTF-8') as json_file:
        return load(json_file)


def missing_env_vars(env_vars):
    """Check if any env var is not configured in the environment."""
    missing_env_vars_locate = [
        env_var
        for env_var in env_vars
        if getenv(env_var, 'missing') == 'missing'
    ]
    return missing_env_vars_locate
