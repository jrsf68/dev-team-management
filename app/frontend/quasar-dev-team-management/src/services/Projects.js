import useApi from 'src/composables/UseApi'

export default function projectsService () {
  const { list, post, remove, update } = useApi('/projects')

  return {
    list,
    post,
    remove,
    update
  }
}
