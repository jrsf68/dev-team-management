const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('pages/IndexPage.vue')
      },
      {
        path: 'configuration',
        name: 'configuration',
        component: () => import('pages/configuration/ConfigurationPage.vue')
      },
      {
        path: 'production',
        name: 'production',
        component: () => import('pages/production/ProductionPage.vue')
      },
      {
        path: 'projects',
        name: 'projects',
        component: () => import('pages/projects/ProjectsPage.vue')
      },
      {
        path: 'team',
        name: 'team',
        component: () => import('pages/team/TeamPage.vue')
      },
      {
        path: 'account',
        name: 'account',
        component: () => import('pages/account/LoginPage.vue'),
        children: [
          {
            path: '',
            name: 'loginpage',
            component: () => import('pages/account/LoginPage.vue')
          },
          {
            path: 'login',
            name: 'login',
            component: () => import('pages/account/LoginPage.vue')
          }
        ]
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
