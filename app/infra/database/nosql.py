# from os import getenv, path
# from logging import getLogger
# from typing import Optional

# from dotenv import load_dotenv
# from pymongo import MongoClient
# from pymongo.database import Database

# logger = getLogger(__name__)
# logger.propagate = False


# load_dotenv('.env')
# load_dotenv('.env.secrets')

# PATH = path.abspath('.')
# PATH += getenv('NOSQL_DB_PATH', '')
# DB_STRING_CONNETION = getenv('NOSQL_DBMS', '')
# DB_STRING_CONNETION += getenv('NOSQL_DBMS_DRIVE', '')
# DB_STRING_CONNETION += '://' + getenv('NOSQL_DB_USERNAME', '')
# DB_STRING_CONNETION += getenv('NOSQL_DB_PASSWORD', '')
# DB_STRING_CONNETION += '' if getenv('NOSQL_DB_USERNAME', '') == '' else '@'
# DB_STRING_CONNETION += getenv('NOSQL_DB_HOSTNAME', '')
# DB_STRING_CONNETION += ':' + getenv('NOSQL_DB_PORT', '')
# # DB_STRING_CONNETION += '/' + getenv('NOSQL_DATABASE_NAME', '')


# async def database_connection(database) -> Database:
#     MONGODB_URL = DB_STRING_CONNETION
#     client = MongoClient(MONGODB_URL)
#     return client[database]


# async def read_collection(
#     collection: str, filter: Optional[str] | None = None
# ) -> list:
#     database = getenv('NOSQL_DATABASE_NAME', '')
#     db_connection: Database = await database_connection(database)
#     collect = db_connection.get_collection(collection)
#     logger.debug(collect)
#     cursor = (
#         collect.find({'tool': filter})
#         if filter is not None
#         else collect.find()
#     )
#     return list(cursor)


# async def read_total_tasks_performed():
#     collection = 'total_tasks_performed'
#     dicio = await read_collection(collection)
#     result_set = []
#     for document in dicio:
#         document['_id'] = str(document['_id'])
#         result_set.append(document)
#     return result_set
