from logging import getLogger
from os import getenv, path
from sqlite3 import connect

from dotenv import load_dotenv
from pandas import DataFrame, read_sql_query

# from app.domain.entities import Developers

logger = getLogger(__name__)
logger.propagate = False


load_dotenv('.env')
load_dotenv('.env.secrets')

PATH = path.abspath('.')
PATH += getenv('DB_PATH', '')

if getenv('DB_DBMS') == 'sqlite3':
    DB_STRING_CONNETION = PATH
else:
    DB_STRING_CONNETION = getenv('DB_DBMS', '')
    DB_STRING_CONNETION += getenv('DB_DBMS_DRIVE', '')
    DB_STRING_CONNETION += '://' + getenv('DB_USERNAME', '')
    DB_STRING_CONNETION += getenv('DB_PASSWORD', '')
    DB_STRING_CONNETION += getenv('DB_HOSTNAME', '')

DB_STRING_CONNETION += getenv('DB_PORT', '')
DB_STRING_CONNETION += '/' + getenv('DB_DATABASE_NAME', '')

if getenv('DB_DBMS') == 'sqlite3':
    DB_STRING_CONNETION += '.db'


async def database_connection():
    try:
        conn = connect(DB_STRING_CONNETION)
        return conn
    except ConnectionError as e:
        logger.error(
            'Database {%s} connection error: {%s}', DB_STRING_CONNETION, e
        )
    return None


async def persist_one_record(record: dict):
    return True


async def persist_table_to_database(table: str, df: DataFrame):
    conn = await database_connection()
    if conn:
        cursor = conn.cursor()
        df.to_sql(name=table, con=conn, if_exists='append', index=False)
        conn.close()
        return True
    return False


async def persist_row_to_database(table: str, field: str, condition: str):
    query = f'UPDATE {table} SET {field} WHERE {condition}'
    conn = await database_connection()
    if conn:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        conn.close()
        return True
    return False


async def read_table_from_database(table: str, filter='') -> list:
    conn = await database_connection()
    if conn:
        filter_sentence = 'WHERE ' + filter if filter else ''
        query = f'SELECT * FROM {table} {filter_sentence}'
        df = read_sql_query(query, conn)
        rows = df.to_dict(orient='records')
        conn.close()
        return rows
    return False  # type: ignore
