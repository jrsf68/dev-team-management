from datetime import datetime

from pandas import DataFrame

from app.domain.entities import Tasks
from app.domain.projects import update_project
from app.infra.database.persistence import persist_table_to_database


async def save_tasks(repositories, tasks):
    df = DataFrame(tasks, columns=list(Tasks.model_fields.keys()))
    result = await persist_table_to_database('tasks', df)
    if result:
        df_repository_last_read_date = (
            df[['tool', 'project', 'completion_date']]
            .groupby(['tool', 'project'])
            .max()
        )
        for index, row in df_repository_last_read_date.iterrows():
            repository = [
                repository
                for repository in repositories
                if 'name' in repository and repository['name'] == index[1]
            ]
            repository[0]['last_read_date'] = datetime.fromisoformat(
                row['completion_date']
            ).strftime('%Y-%m-%d %H:%M:%S')

            await update_project(repository[0])
    return result
