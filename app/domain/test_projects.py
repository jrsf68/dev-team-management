"""Tests project functions.
"""
import uuid
from os import getenv

import pytest

from app.domain.entities import ManagementTools
from app.domain.projects import (
    get_projects_from_bitbucket,
    get_projects_from_gitlab,
    get_projects_from_jira,
    get_projects_from_taiga,
    read_projects,
    save_projects,
)


@pytest.mark.asyncio
async def test_get_projects_from_bitbucket():
    """Read new projects from Bitbucket."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='bitbucket',
        username=getenv('BITBUCKET_USERNAME', ''),
        password=getenv('BITBUCKET_PASSWORD', ''),
        token='',
        login_url='',
        data_url=getenv('BITBUCKET_DATA_URL', ''),
    )
    registered_projects = await read_projects(filter='tool="bitbucket"')
    projects = await get_projects_from_bitbucket(
        management_tool, registered_projects
    )
    if projects:
        await save_projects(projects)

    assert projects != 'Error'


@pytest.mark.asyncio
async def test_get_projects_from_gitlab():
    """Read new projects from Gitlab."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='gitlab',
        username=getenv('GITLAB_USERNAME'),
        password='',
        token=getenv('GITLAB_TOKEN'),
        login_url=getenv('GITLAB_LOGIN_URL'),
        data_url='',
    )

    projects = await get_projects_from_gitlab(management_tool)
    await save_projects(projects)

    assert projects


@pytest.mark.asyncio
async def test_get_projects_from_jira():
    """Read new projects from Jira."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='jira',
        username=getenv('JIRA_USER_EMAIL'),
        password='',
        token=getenv('JIRA_TOKEN'),
        login_url=getenv('JIRA_SERVER'),
        data_url='',
    )

    projects = await get_projects_from_jira(management_tool)
    await save_projects(projects)

    assert projects


@pytest.mark.asyncio
async def test_get_projects_from_taiga():
    """Read new projects from Taiga."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='taiga',
        username=getenv('TAIGA_USERNAME'),
        password=getenv('TAIGA_PASSWORD'),
        token='',
        login_url=getenv('TAIGA_SERVER'),
        data_url='',
    )
    projects = await get_projects_from_taiga(management_tool)
    await save_projects(projects)

    assert projects != 'Error'


# @pytest.mark.asyncio
# async def test_save_projects():
#     projects = [
#         {
#             'uuid': str(uuid.uuid4()),
#             'id': '0',
#             'key': '',
#             'tool': 'taigason',
#             'name': 'projeto_teste',
#             'parent': 'projeto_teste',
#             'last_read_date': datetime.strptime(
#                 '2023-06-01 00:00:00', '%Y-%m-%d %H:%M:%S'
#             ),
#             'status': 'Active',
#         }
#     ]

#     projects_saved = await save_projects(projects)

#     assert projects_saved


# @pytest.mark.asyncio
# async def test_read_projects():
#     filter = 'tool="taigason"'
#     projects = await read_projects(filter=filter)
#     assert projects
