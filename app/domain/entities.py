"""Development team management schemas
"""

from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class ManagementTools(BaseModel):
    uuid: Optional[UUID]
    tool: str
    username: str
    password: str
    token: str = ''
    login_url: str = ''
    data_url: str = ''


class Developers(BaseModel):
    uuid: Optional[UUID]
    full_name: Optional[str]
    email: Optional[str]
    gitlab_id: Optional[int]
    gitlab_user: Optional[str]
    gitlab_email: Optional[str]
    taiga_username: Optional[str]
    bitbucket_name: Optional[str]
    bitbucket_user: Optional[str]
    bitbucket_email: Optional[str]
    jira_name: Optional[str]
    jira_name_2: Optional[str]
    jira_project_manager: Optional[int] = 0
    status: str = 'Active'


class Projects(BaseModel):
    uuid: Optional[UUID]
    id: Optional[str]
    key: Optional[str]
    tool: Optional[str]
    name: Optional[str]
    parent: Optional[str]
    last_read_date: datetime
    status: str = 'Active'


class Tasks(BaseModel):
    uuid: Optional[UUID]
    tool: Optional[str]
    developer_full_name: Optional[str]
    developer_email: Optional[str]
    developer_name: Optional[str]
    project: Optional[str]
    task_id: Optional[str]
    task: Optional[str]
    complement: Optional[str]
    completion_date: datetime
    status: Optional[str]
