"""Implements developer registration functions
"""
import uuid
from os import getenv

import requests
from dotenv import load_dotenv
from pytest import mark

from app.domain.developers import get_developers_from_gitlab
from app.domain.entities import ManagementTools

load_dotenv('.env')
load_dotenv('.env.secrets')


# @mark.asyncio
# async def test_get_developers():
#     """Get users from taiga tool."""
#     server = {
#         'base_url': 'https://taiga.prodepa.pa.gov.br/api/v1',
#         'username': getenv('TAIGA_USERNAME', ''),
#         'password': getenv('TAIGA_PASSWORD', ''),
#     }
#     token_endpoint = f"{server['base_url']}/auth"
#     credential = {
#         'type': 'normal',
#         'username': server['username'],
#         'password': server['password'],
#     }
#     response = requests.post(token_endpoint, data=credential, timeout=10)
#     access_token = response.json()['auth_token']
#     headers = {'Authorization': f'Bearer {access_token}'}
#     offset = 0
#     page=1
#     while True:
#         print(f'page: {page}')
#         endpoint = f"{server['base_url']}/users?page={page}"
#         response = requests.get(endpoint, headers=headers, timeout=100)
#         if response.status_code == 200:
#             users = response.json()
#             for user in users:
#                 print(
#                     f'full_name: {user["full_name"]} - id: {user["id"]} - username: {user["username"]} - is_active: {user["is_active"]}'
#                 )
#             page += 1
#         else:
#             break
#     assert True


@mark.asyncio
async def test_get_developers_from_gitlab():
    """Read new projects from Gitlab."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='gitlab',
        username=getenv('GITLAB_USERNAME'),
        password='',
        token=getenv('GITLAB_TOKEN'),
        login_url=getenv('GITLAB_LOGIN_URL'),
        data_url='',
    )

    projects = await get_developers_from_gitlab(management_tool)
    assert False


# @mark.asyncio
# async def test_register_a_new_developer():
#     """test to insert a new developer"""
#     developer = {
#         'full_name': 'new developer',
#         'email': 'new_developer@new.dev.app',
#         'status': 'Active',
#     }
#     result = await register_a_new_developer(developer)
#     assert result


# @pytest.mark.asyncio
# async def test_save_developers():
#     developers = [
#         {
#             'full_name': 'TEST DEVELOPER',
#             'email': 'test@developer.com.br',
#             'gitlab_id': 1000,
#             'gitlab_user': 'gitlab.test.developer',
#             'gitlab_email': 'test@developer.com.br',
#             'taiga_username': 'taiga.test.developer',
#             'bitbucket_name': 'bitbucket.test.developer',
#             'bitbucket_user': 'bitbucket.test.developer',
#             'jira_name': 'jira.test.developer',
#             'jira_name_2': 'jira2.test.developer',
#         }
#     ]

#     result = await save_developers(developers)

#     assert result
