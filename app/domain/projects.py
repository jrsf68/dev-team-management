import uuid
from datetime import datetime
from logging import getLogger

import gitlab
from dotenv import load_dotenv
from jira import JIRA
from pandas import DataFrame
from requests import get, post

logger = getLogger(__name__)
logger.propagate = False


from app.domain.entities import ManagementTools, Projects
from app.infra.database.persistence import (
    persist_row_to_database,
    persist_table_to_database,
    read_table_from_database,
)

load_dotenv('.env')
load_dotenv('.env.secrets')


async def get_projects_from_bitbucket(
    management_tool: ManagementTools, registered_projects: list
):
    username = management_tool.username
    password = management_tool.password
    url = management_tool.data_url
    headers = {'Content-Type': 'application/json'}
    repositories = []
    next_page = url
    while next_page:
        response = get(next_page, auth=(username, password), headers=headers)
        if response.status_code == 200:
            all_repositories = response.json()['values']
            read_repositories = [
                {
                    'uuid': str(uuid.uuid4()),
                    'id': repository['uuid'],
                    'key': '',
                    'tool': 'bitbucket',
                    'name': repository['full_name'].rsplit('/', 1)[-1],
                    'parent': repository['full_name'].rsplit('/', 1)[-1],
                    'last_read_date': '2024-03-15 00:00:00',
                    'status': 'Active',
                }
                for repository in all_repositories
                if datetime.strptime(
                    repository['updated_on'], '%Y-%m-%dT%H:%M:%S.%f%z'
                )
                > datetime.strptime(
                    '2023-01-01T00:00:01.000000+00:00',
                    '%Y-%m-%dT%H:%M:%S.%f%z',
                )
                and repository['uuid']
                not in [project['id'] for project in registered_projects]
            ]
            repositories.extend(read_repositories)
        next_page = (response.json()).get('next')
    return repositories


async def get_projects_from_gitlab(management_tool: ManagementTools):
    project_list = []
    if management_tool.tool == 'gitlab':
        attempt = 1
        while True:
            try:
                gitlab_connection = gitlab.Gitlab(
                    management_tool.login_url,
                    private_token=management_tool.token,
                    ssl_verify=False,
                    timeout=500,
                )
                projects_read = gitlab_connection.projects.list(
                    all_users=True, all=True
                )
                project_list = [
                    {
                        'uuid': str(uuid.uuid4()),
                        'id': str(vars(project)['_attrs']['id']),
                        'key': '',
                        'tool': management_tool.tool,
                        'name': vars(project)['_attrs']['path'],
                        'parent': vars(project)['_attrs']['path'],
                        'last_read_date': datetime.strptime(
                            vars(project)['_attrs']['last_activity_at'],
                            '%Y-%m-%dT%H:%M:%S.%f%z',
                        ).strftime('%Y-%m-%d %H:%M:%S'),
                        'status': 'Active',
                    }
                    for project in projects_read
                    if datetime.strptime(
                        vars(project)['_attrs']['last_activity_at'],
                        '%Y-%m-%dT%H:%M:%S.%fz',
                    )
                    > datetime.strptime(
                        '2024-04-01T00:00:00.000z', '%Y-%m-%dT%H:%M:%S.%fz'
                    )
                ]
                break
            except Exception as e:
                attempt += 1
            if attempt == 6:
                logger.error('Error reading gitlab projects: {%s}', e)
                return False
    return project_list


async def get_projects_from_jira(management_tool: ManagementTools) -> list:
    jiraOptions = {'server': management_tool.login_url}
    jira = JIRA(
        options=jiraOptions,
        basic_auth=(management_tool.username, management_tool.token),
    )

    projects = jira.projects()
    list_of_jira_projects = [
        {
            'uuid': str(uuid.uuid4()),
            'id': project.raw['id'],
            'key': project.raw['key'],
            'tool': 'jira',
            'name': project.raw['name'],
            'parent': project.raw['name'],
            'last_read_date': '2024-03-15 00:00:00',
            'status': 'Active',
        }
        for project in projects
    ]
    return list_of_jira_projects


async def get_projects_from_taiga(management_tool: ManagementTools) -> list:
    """Read e return list of Taiga projects.

    Args:
        management_tool (ManagementTools): Class with Server connection attributes.

    Returns:
        list: list of dictionay contain project's 'tool', 'id', 'name' and 'last_read_date'.
    """
    token_endpoint = f'{management_tool.login_url}/auth'
    data = {
        'type': 'normal',
        'username': management_tool.username,
        'password': management_tool.password,
    }
    attempt = 1
    while True:
        try:
            response = post(token_endpoint, data=data, timeout=500)
            break
        except Exception as e:
            attempt += 1
        if attempt == 5:
            logger.error(f'Error reading {token_endpoint}: ({e})')
            return 'Error'
    management_tool.token = response.json()['auth_token']
    headers = {'Authorization': f'Bearer {management_tool.token}'}
    projects_endpoint = f'{management_tool.login_url}/projects'
    projects_response = get(projects_endpoint, headers=headers)
    projects = projects_response.json()
    taiga_project_list = [
        {
            'uuid': str(uuid.uuid4()),
            'id': str(project['id']),
            'key': '',
            'tool': 'taiga',
            'name': project['name'],
            'parent': project['name'],
            'last_read_date': '2024-03-15 00:00:00',
            'status': 'Active',
        }
        for project in projects
        if datetime.strptime(
            project['modified_date'],
            '%Y-%m-%dT%H:%M:%S.%fz'
            if '.' in project['modified_date']
            else '%Y-%m-%dT%H:%M:%Sz',
        )
        > datetime.strptime(
            '2022-01-01T00:00:00.000z', '%Y-%m-%dT%H:%M:%S.%fz'
        )
    ]
    return taiga_project_list


async def save_projects(projects):
    df = DataFrame(projects, columns=list(Projects.model_fields.keys()))
    projects_read = await read_projects()
    if projects_read:
        df_read = DataFrame(projects_read)
        merged_df = df.merge(
            df_read,
            on=['tool', 'id', 'name'],
            how='left',
            indicator=True,
        )
        diff_df = merged_df.loc[merged_df['_merge'] == 'left_only'].drop(
            '_merge',
            axis=1,
        )
        remove_cols_df = diff_df.drop(
            ['uuid_y', 'key_y', 'parent_y', 'last_read_date_y', 'status_y'],
            axis=1,
        )
        new_projects = remove_cols_df.rename(
            columns={
                'uuid_x': 'uuid',
                'key_x': 'key',
                'parent_x': 'parent',
                'last_read_date_x': 'last_read_date',
                'status_x': 'status',
            }
        )
    else:
        new_projects = df
    if (
        not new_projects.empty
        and not new_projects.isna().sum(axis=1).eq(new_projects.shape[1]).sum()
        == 1
    ):
        result = await persist_table_to_database('projects', new_projects)
    else:
        return False
    return result


async def read_projects(filter: str = None):
    projects = await read_table_from_database(table='projects', filter=filter)
    return projects


async def update_project(project):
    table = 'projects'
    field = f'last_read_date = "{ project["last_read_date"] }"'
    condition = f'tool = "{ project["tool"] }" AND id = "{ project["id"] }" AND name = "{ project["name"] }"'
    result = await persist_row_to_database(
        table=table, field=field, condition=condition
    )
    return result
