import logging

import gitlab
from pandas import DataFrame

from app.domain.entities import Developers, ManagementTools
from app.infra.database.persistence import (
    persist_one_record,
    persist_table_to_database,
    read_table_from_database,
)


async def register_a_new_developer(developer: dict):
    """Insert a new developer"""
    result = await persist_one_record(developer)
    return result


async def read_developers() -> list:
    developers = await read_table_from_database('developers')
    return developers


async def save_developers(developers):
    df = DataFrame(developers, columns=list(Developers.model_fields.keys()))

    result = await persist_table_to_database('developers', df)

    return result


async def get_developers_from_gitlab(management_tool: ManagementTools):
    if management_tool.tool == 'gitlab':
        try:
            gitlab_connection = gitlab.Gitlab(
                management_tool.login_url,
                private_token=management_tool.token,
                ssl_verify=False,
                timeout=500,
            )
            users_read = gitlab_connection.users.list(all=True, per_page=1000)
            for user in users_read:
                # print(vars(user))
                print(
                    f"state: {vars(user)['_attrs']['state']} - name: {vars(user)['_attrs']['name']} - id: {vars(user)['_attrs']['username']} - id: {vars(user)['_attrs']['id']}"
                )
        except Exception as e:
            logging.error('Error reading gitlab projects: {%s}', e)
            return False
    return False
