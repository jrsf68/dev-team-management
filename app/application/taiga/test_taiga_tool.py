"""Tests of Taiga tool data extract functions.
"""
import uuid
from os import getenv

import pytest
from dotenv import load_dotenv

from app.application.taiga.taiga_tool import get_all_taiga_project_tasks
from app.domain.developers import read_developers
from app.domain.entities import ManagementTools
from app.domain.projects import read_projects
from app.domain.tasks import save_tasks

load_dotenv('.env')
load_dotenv('.env.secrets')


@pytest.mark.asyncio
async def test_get_all_taiga_project_tasks():
    """Test if get project tasks is working."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='taiga',
        username=getenv('TAIGA_USERNAME', ''),
        password=getenv('TAIGA_PASSWORD', ''),
        token='',
        login_url=getenv('TAIGA_SERVER', ''),
        data_url='',
    )
    projects = await read_projects(filter='tool="taiga"')
    developer_registration = await read_developers()
    developers = [
        {
            'full_name': developer['full_name'],
            'taiga_id': developer['taiga_id'],
        }
        for developer in developer_registration
        if developer['taiga_id'] != '0'
    ]

    taiga_tasks = await get_all_taiga_project_tasks(
        management_tool, projects, developers
    )
    taiga_tasks_saved = await save_tasks(projects, taiga_tasks)

    assert taiga_tasks_saved != 'Error'
