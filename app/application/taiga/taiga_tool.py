"""Taiga tool data extract functions.
"""
import uuid
from datetime import datetime
from logging import getLogger
from time import sleep

from pandas import DataFrame
from requests import Response, exceptions, get, post

from app.domain.entities import ManagementTools
from app.infra.database.persistence import persist_table_to_database

logger = getLogger(__name__)
logger.propagate = False


async def taiga_connection(taiga_server: ManagementTools) -> Response:
    """Connect to Taiga  Web Server.
    Args:
        taiga_server (ManagementTools): Class with Taiga Server connection attributes.
    Returns:
        Response: Object with response data from HTTP request.
    """
    token_endpoint = f'{taiga_server.login_url}/auth'
    data = {
        'type': 'normal',
        'username': taiga_server.username,
        'password': taiga_server.password,
    }
    retries = 5
    for _ in range(retries):
        try:
            response = post(token_endpoint, data=data, timeout=500)
            if response.status_code == 200:
                break
        except ConnectionError as e:
            logger.critical('Connection error (%s).', e)
        except ConnectionResetError as e:
            logger.critical('Connection error (%s).', e)
        sleep(5)
    return response


async def get_all_taiga_project_tasks(management_tool, projects, developers):
    """Get tasks from Taiga projects."""
    all_tasks = []
    response = await taiga_connection(taiga_server=management_tool)
    management_tool.token = response.json()['auth_token']
    for project in projects:
        task_list = await get_taiga_project_tasks(
            management_tool, project, developers
        )
        if task_list == 'Error':
            return task_list
        if task_list:
            all_tasks.extend(task_list)
    return all_tasks


async def get_taiga_project_tasks(
    taiga_server: ManagementTools, project: dict, developers: list
) -> list:
    """Reads and returns the list of Taiga projects.

    Args:
        taiga_server (TaigaServers): Class with Taiga Server connection attributes.
        project (dict): list of dictionary contain project's 'id' and 'name'.
        developers (list): list of developers.
    Returns:
        list: list of dictionary contain project tasks or None.
    """
    page = 0
    all_tasks = []
    developers_id = [taiga_id['taiga_id'] for taiga_id in developers]
    headers = {'Authorization': f'Bearer {taiga_server.token}'}
    while True:
        page += 1
        tasks_endpoint = f'{taiga_server.login_url}/tasks?project={project["id"]}&page={page}'
        try:
            response_tasks = get(tasks_endpoint, headers=headers)
        except exceptions.RequestException as e:
            logger.error('Error reading task page ({%s})', e)
            return []
        if response_tasks.status_code == 200:
            json_tasks = response_tasks.json()
            if not json_tasks:
                break
            for task in json_tasks:
                try:
                    task_endpoint = (
                        f'{taiga_server.login_url}/history/task/{task["id"]}'
                    )
                    response_task = get(task_endpoint, headers=headers)
                except exceptions.RequestException as e:
                    logger.error('Error reading task page ({%s})', e)
                    continue
                json_task = response_task.json()
                if not json_task:
                    continue
                for comment in json_task:
                    if (
                        datetime.fromisoformat(comment['created_at']).strftime(
                            '%Y-%m-%d %H:%M:%S'
                        )
                        <= project['last_read_date']
                    ):
                        continue
                    if comment['user']['pk'] not in developers_id:
                        continue
                    if not comment['comment']:
                        continue
                    comment_in_line = (
                        comment['comment'].replace('\n', '').replace('  ', '')
                    )
                    description = f"#{task['ref']} - "
                    description += f'{comment_in_line} - '
                    description += f"{task['status_extra_info']['name']}"
                    all_tasks.append(
                        {
                            'uuid': str(uuid.uuid4()),
                            'tool': 'taiga',
                            'developer_full_name': [
                                developer['full_name']
                                for developer in developers
                                if developer['taiga_id']
                                == comment['user']['pk']
                            ][0],
                            'developer_email': '',
                            'developer_name': '',
                            'project': task['project_extra_info']['name'],
                            'task_id': str(task['id']),
                            'task': description,
                            'complement': '',
                            'completion_date': datetime.fromisoformat(
                                comment['created_at']
                            ).strftime('%Y-%m-%d %H:%M:%S'),
                            'project_date': project['last_read_date'],
                            'status': task['status_extra_info']['name'],
                        }
                    )
        else:
            if page <= 1:
                logger.info("Project don't have task: %s", response_tasks.text)
            break
    page = 0
    while True:
        page += 1
        issues_endpoint = f'{taiga_server.login_url}/issues?project={project["id"]}&page={page}'
        try:
            response_issues = get(issues_endpoint, headers=headers)
        except exceptions.RequestException as e:
            logger.error('Error reading issue page ({%s})', e)
            return []
        if response_issues.status_code == 200:
            json_issues = response_issues.json()
            if not json_issues:
                break
            for issue in json_issues:
                try:
                    issue_endpoint = (
                        f'{taiga_server.login_url}/history/issue/{issue["id"]}'
                    )
                    response_issue = get(issue_endpoint, headers=headers)
                except exceptions.RequestException as e:
                    logger.error('Error reading issue page ({%s})', e)
                    continue
                json_issue = response_issue.json()
                if not json_issue:
                    continue
                for comment in json_issue:
                    # print(f"issue['project: {issue['project_extra_info']['name']}")
                    # print(f"issue['id']: {issue['id']}")
                    # print(f"issue['ref']: {issue['ref']}")
                    if (
                        datetime.fromisoformat(comment['created_at']).strftime(
                            '%Y-%m-%d %H:%M:%S'
                        )
                        <= project['last_read_date']
                    ):
                        continue
                    if comment['user']['pk'] not in developers_id:
                        continue
                    if not comment['comment']:
                        continue
                    comment_in_line = (
                        comment['comment'].replace('\n', '').replace('  ', '')
                    )
                    description = f"#{issue['ref']} - "
                    description += f'{comment_in_line} - '
                    description += f"{issue['status_extra_info']['name']}"
                    all_tasks.append(
                        {
                            'uuid': str(uuid.uuid4()),
                            'tool': 'taiga',
                            'developer_full_name': [
                                developer['full_name']
                                for developer in developers
                                if developer['taiga_id']
                                == comment['user']['pk']
                            ][0],
                            'developer_email': '',
                            'developer_name': '',
                            'project': issue['project_extra_info']['name'],
                            'task_id': str(issue['id']),
                            'task': description,
                            'complement': '',
                            'completion_date': datetime.fromisoformat(
                                comment['created_at']
                            ).strftime('%Y-%m-%d %H:%M:%S'),
                            'project_date': project['last_read_date'],
                            'status': issue['status_extra_info']['name'],
                        }
                    )
        else:
            if page <= 1:
                logger.info(
                    "Project don't have issue: %s", response_issues.text
                )
            break
    return all_tasks


async def save_taiga_tasks(tasks_read: list) -> bool:
    """Save task list in database.

    Args:
        tasks_read (list): list of dictionary contain project tasks.

    Returns:
        bool: True if saved correctly.
    """
    df = DataFrame(
        tasks_read,
        columns=['developer', 'service', 'task', 'task_date', 'status'],
    )

    result = await persist_table_to_database('tasks', df)

    return result
