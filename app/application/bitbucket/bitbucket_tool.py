"""Bitbucket tool procedures implementation"""

import re
import uuid
from datetime import datetime
from logging import getLogger

from requests import get

from app.domain.entities import ManagementTools

logger = getLogger(__name__)
logger.propagate = False


async def get_commits_from_bitbucket(
    management_tool: ManagementTools, repository: dict, developers: list
):
    """Read commits from bitbucket"""
    now = datetime.now()
    url = f'{management_tool.data_url}/{repository["name"]}/commits'

    try:
        response = get(
            url,
            auth=(management_tool.username, management_tool.password),
            timeout=5000,
        )
        if response.status_code != 200:
            return []
        commits = response.json()['values']
        list_commits = []
        for commit in commits:
            author_email = re.search(r'<(.+?)>', commit['author']['raw'])
            author_email = author_email.group(1) if author_email else ''
            if (
                commit['type'] == 'commit'
                and commit['message'][:14] != "Merge branch '"
                and [
                    developer[1]
                    for developer in developers
                    if developer[1] == author_email
                ]
                and datetime.fromisoformat(commit['date']).strftime(
                    '%Y-%m-%d %H:%M:%S'
                )
                > repository['last_read_date']
                and datetime.fromisoformat(commit['date']).strftime(
                    '%Y-%m-%d %H:%M:%S'
                )
                < now.strftime('%Y-%m-%d %H:%M:%S')
            ):
                commit_read = {}
                commit_read['uuid'] = str(uuid.uuid4())
                commit_read['tool'] = 'bitbucket'
                commit_read['developer_full_name'] = [
                    developer[0]
                    for developer in developers
                    if developer[1] == author_email
                ][0]
                commit_read['developer_email'] = author_email
                commit_read['project'] = commit['repository'][
                    'full_name'
                ].rsplit('/', 1)[-1]
                commit_read['task_id'] = commit['hash']
                commit_read['task'] = (commit['message']).replace('\n', '')
                commit_read['completion_date'] = datetime.fromisoformat(
                    commit['date']
                ).strftime('%Y-%m-%d %H:%M:%S')
                commit_read['status'] = 'Complited'
                list_commits.append(commit_read)
        return list_commits
    except ExceptionGroup as e:
        logger.debug('Error reading from {%s} ({%s})', repository['name'], e)
        return 'Error'


async def get_commits_from_bitbucket_repositories(
    management_tool: ManagementTools, repositories: list, developers: list
):
    """Extract commits from all bitbucket repositories"""
    all_tasks = []
    for repository in repositories:
        bitbucket_tasks = await get_commits_from_bitbucket(
            management_tool, repository, developers
        )
        if bitbucket_tasks == 'Error':
            return bitbucket_tasks
        if bitbucket_tasks == []:
            continue
        all_tasks.extend(bitbucket_tasks)
    return all_tasks
