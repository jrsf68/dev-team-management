"""Test main program"""

import uuid
from os import getenv

import pytest
from dotenv import load_dotenv

from app.application.bitbucket.bitbucket_tool import (
    get_commits_from_bitbucket_repositories,
)
from app.domain.developers import read_developers
from app.domain.entities import ManagementTools
from app.domain.projects import read_projects
from app.domain.tasks import save_tasks

load_dotenv('.env')
load_dotenv('.env.secrets')


@pytest.mark.asyncio
async def test_save_commits_from_bitbucket():
    """Persists data from bitbucket commits."""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='bitbucket',
        username=getenv('BITBUCKET_USERNAME', ''),
        password=getenv('BITBUCKET_PASSWORD', ''),
        token='',
        login_url='',
        data_url=getenv('BITBUCKET_DATA_URL', ''),
    )
    repositories = await read_projects(filter='tool="bitbucket"')
    developer_registration = await read_developers()
    developers = [
        [developer['full_name'], developer['bitbucket_email']]
        for developer in developer_registration
        if developer['bitbucket_email']
    ]
    commits = await get_commits_from_bitbucket_repositories(
        management_tool, repositories, developers
    )

    commits_saved = await save_tasks(repositories, commits)

    assert commits_saved
