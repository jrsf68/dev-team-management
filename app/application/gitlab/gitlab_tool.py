"""gitlab scrape function implementation
"""
import uuid
from datetime import datetime
from logging import getLogger
from time import sleep

import gitlab

from app.domain.entities import ManagementTools

logger = getLogger(__name__)
logger.propagate = False


async def get_events_from_gitlab_projects(
    management_tool: ManagementTools, repositories: list, developers: list
):
    """Extract commits from all gitlab repositories"""
    all_events = []
    attempt = 1
    while True:
        try:
            gitlab_connection = gitlab.Gitlab(
                management_tool.login_url,
                private_token=management_tool.token,
                ssl_verify=False,
                timeout=500,
            )
            break
        except Exception as e:
            attempt += 1
            sleep(5)
        if attempt == 6:
            logger.error('Gitlab connection error: {%s}', e)
            return False
    for repository in repositories:
        # if repository['name'] != 'delegacia_virtual_frontend':
        #     continue
        gitlab_commits = await get_commits_from_gitlab(
            gitlab_connection, repository, developers
        )
        if gitlab_commits != [] and gitlab_commits != 'Error':
            all_events.extend(gitlab_commits)
    if gitlab_commits != [] and gitlab_commits != 'Error':
        all_events.extend(gitlab_commits)

    return all_events


async def get_commits_from_gitlab(
    gitlab_connection, repository: dict, developers: list
):
    """Get commits from a specific gitlab's repository."""
    try:
        filtered_events = []
        projs = gitlab_connection.projects.list()
        project_read_from_tool_server = gitlab_connection.projects.get(
            repository['id']
        )
        get_all_commits = project_read_from_tool_server.commits.list(
            get_all=True, all=True
        )
        filtered_events = [
            {
                'uuid': str(uuid.uuid4()),
                'tool': 'gitlab',
                'developer_full_name': [
                    developer['full_name']
                    for developer in developers
                    if (
                        developer['gitlab_user']
                        == vars(commit)['_attrs']['committer_name']
                        or developer['gitlab_email']
                        == vars(commit)['_attrs']['committer_email']
                    )
                ][0],
                'developer_email': vars(commit)['_attrs']['committer_email'],
                'developer_name': vars(commit)['_attrs']['committer_name'],
                'project': repository['name'],
                'task_id': vars(commit)['_attrs']['short_id'],
                'task': '#'
                + vars(commit)['_attrs']['short_id']
                + ' - '
                + vars(commit)['_attrs']['message']
                .rstrip('\n')
                .replace('\n', '-')
                if vars(commit)['_attrs']['title'].replace('\n', '')[:-3]
                in vars(commit)['_attrs']['message'].replace('\n', '')
                else vars(commit)['_attrs']['title']
                .rstrip('\n')
                .replace('\n', '-'),
                'complement': 'commit'
                if vars(commit)['_attrs']['title'].replace('\n', '')[:-3]
                in vars(commit)['_attrs']['message'].replace('\n', '')
                else vars(commit)['_attrs']['message']
                .rstrip('\n')
                .replace('\n', '-')
                + 'commit',
                'completion_date': datetime.fromisoformat(
                    vars(commit)['_attrs']['committed_date']
                ).strftime('%Y-%m-%d %H:%M:%S'),
                'status': 'Complited',
            }
            for commit in get_all_commits
            if vars(commit)['_attrs']['title'][:14] != "Merge branch '"
            and vars(commit)['_attrs']['title'][:12] != 'Merge remote'
            and datetime.fromisoformat(
                vars(commit)['_attrs']['committed_date']
            ).strftime('%Y-%m-%d %H:%M:%S')
            > repository['last_read_date']
            and next(
                (
                    developer
                    for developer in developers
                    if developer['gitlab_user']
                    == vars(commit)['_attrs']['committer_name']
                    or developer['gitlab_email']
                    == vars(commit)['_attrs']['committer_email']
                ),
                None,
            )
            is not None
        ]

        get_all_issues = project_read_from_tool_server.issues.list(
            get_all=True, all=True
        )
        issue_id_list = [vars(id)['_attrs']['iid'] for id in get_all_issues]
        for issue_number in issue_id_list:
            issue = [
                issue
                for issue in get_all_issues
                if vars(issue)['_attrs']['iid'] == issue_number
            ][0]
            page_number = 1
            per_page = 100
            while True:
                try:
                    notes = project_read_from_tool_server.issues.get(
                        issue_number
                    ).discussions.list(page=page_number, per_page=per_page)
                    if not notes:
                        break
                    page_number += 1
                    for note in notes:
                        if not (
                            (
                                'marked the task '
                                in vars(note)['_attrs']['notes'][0]['body']
                                and ' as completed'
                                in vars(note)['_attrs']['notes'][0]['body']
                            )
                            or 'closed'
                            in vars(note)['_attrs']['notes'][0]['body']
                        ):
                            continue
                        if (
                            (
                                (
                                    (
                                        'marked the task '
                                        in vars(note)['_attrs']['notes'][0][
                                            'body'
                                        ]
                                        and ' as completed'
                                        in vars(note)['_attrs']['notes'][0][
                                            'body'
                                        ]
                                    )
                                    or 'closed'
                                    in vars(note)['_attrs']['notes'][0]['body']
                                )
                                or vars(note)['_attrs']['notes'][0]['body'][
                                    0:13
                                ]
                                != 'assigned to @'
                            )
                            and datetime.fromisoformat(
                                vars(note)['_attrs']['notes'][0]['updated_at']
                            ).strftime('%Y-%m-%d %H:%M:%S')
                            > repository['last_read_date']
                            and [
                                developer['gitlab_user']
                                for developer in developers
                                if developer['gitlab_id']
                                == vars(note)['_attrs']['notes'][0]['author'][
                                    'id'
                                ]
                            ]
                            != []
                        ):
                            filtered_events.append(
                                {
                                    'uuid': str(uuid.uuid4()),
                                    'tool': 'gitlab',
                                    'developer_full_name': [
                                        developer['full_name']
                                        for developer in developers
                                        if (
                                            developer['gitlab_user']
                                            == vars(note)['_attrs']['notes'][
                                                0
                                            ]['author']['username']
                                        )
                                    ][0],
                                    'developer_email': '',
                                    'developer_name': vars(note)['_attrs'][
                                        'notes'
                                    ][0]['author']['username'],
                                    'project': repository['name'],
                                    'task_id': vars(note)['_attrs']['id'],
                                    'task': '#'
                                    + str(
                                        vars(note)['_attrs']['notes'][0]['id']
                                    )
                                    + ' - '
                                    + issue.title
                                    + ': '
                                    + vars(note)['_attrs']['notes'][0][
                                        'body'
                                    ].split('**')[1],
                                    'complement': 'note',
                                    'completion_date': datetime.fromisoformat(
                                        vars(note)['_attrs']['notes'][0][
                                            'updated_at'
                                        ]
                                    ).strftime('%Y-%m-%d %H:%M:%S'),
                                    'status': 'Complited',
                                }
                            )
                except IndexError:
                    if not vars(issue)['_attrs']['assignee']:
                        ...
                    elif (
                        datetime.fromisoformat(
                            vars(issue)['_attrs']['updated_at']
                        ).strftime('%Y-%m-%d %H:%M:%S')
                        <= repository['last_read_date']
                    ):
                        ...
                    elif (
                        vars(issue)['_attrs']['state'] == 'closed'
                        and datetime.fromisoformat(
                            vars(issue)['_attrs']['updated_at']
                        ).strftime('%Y-%m-%d %H:%M:%S')
                        > repository['last_read_date']
                        and [
                            developer['full_name']
                            for developer in developers
                            if developer['gitlab_id']
                            == vars(issue)['_attrs']['author']['id']
                        ]
                        != []
                    ):
                        filtered_events.append(
                            {
                                'uuid': str(uuid.uuid4()),
                                'tool': 'gitlab',
                                'developer_full_name': [
                                    developer['full_name']
                                    for developer in developers
                                    if (
                                        developer['gitlab_user']
                                        == vars(issue)['_attrs']['author'][
                                            'username'
                                        ]
                                    )
                                ][0],
                                'developer_email': '',
                                'developer_name': vars(issue)['_attrs'][
                                    'author'
                                ]['username'],
                                'project': repository['name'],
                                'task_id': vars(issue)['_attrs']['id'],
                                'task': '#'
                                + str(vars(issue)['_attrs']['id'])
                                + ' - '
                                + vars(issue)['_attrs']['title'],
                                'complement': 'issue',
                                'completion_date': datetime.fromisoformat(
                                    vars(issue)['_attrs']['updated_at']
                                ).strftime('%Y-%m-%d %H:%M:%S'),
                                'status': 'Complited',
                            }
                        )
    except NameError as e:
        logger.error(
            'Error reading project {%s} commits: {%s}', repository['id'], e
        )
        return 'Error'
    return filtered_events
