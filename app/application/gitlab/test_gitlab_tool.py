"""gitlab scrape function implementation
"""

import uuid
from os import getenv

import pytest
from dotenv import load_dotenv

from app.application.gitlab.gitlab_tool import get_events_from_gitlab_projects
from app.domain.developers import read_developers
from app.domain.entities import ManagementTools
from app.domain.projects import read_projects
from app.domain.tasks import save_tasks

load_dotenv('.env')
load_dotenv('.env.secrets')


@pytest.mark.asyncio
async def test_get_events_from_gitlab_project():
    """Test get events from gitlab"""
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='gitlab',
        username=getenv('GITLAB_USERNAME', ''),
        password='',
        token=getenv('GITLAB_TOKEN', ''),
        login_url=getenv('GITLAB_LOGIN_URL', ''),
        data_url=getenv('GITLAB_DATA_URL', ''),
    )
    management_tool.data_url.replace(
        'GITLAB_LOGIN_URL', management_tool.login_url
    ).replace('GITLAB_USERNAME', management_tool.username)
    repositories = await read_projects(filter='tool="gitlab"')
    developer_registration = await read_developers()
    developers = [
        {
            'gitlab_id': developer['gitlab_id'],
            'gitlab_user': developer['gitlab_user'],
            'gitlab_email': developer['gitlab_email'],
            'full_name': developer['full_name'],
        }
        for developer in developer_registration
        if developer['gitlab_user']
    ]

    gitlab_events = await get_events_from_gitlab_projects(
        management_tool, repositories, developers
    )

    await save_tasks(repositories, gitlab_events)

    assert gitlab_events != 'Error'
