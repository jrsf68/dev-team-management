import uuid
from os import getenv

import pytest
from dotenv import load_dotenv

from app.application.jira.jira_tool import get_items_from_jira_projects
from app.domain.developers import read_developers
from app.domain.entities import ManagementTools
from app.domain.projects import read_projects
from app.domain.tasks import save_tasks

load_dotenv('.env')
load_dotenv('.env.secrets')


@pytest.mark.asyncio
async def test_get_items_from_jira_projects():
    management_tool = ManagementTools(
        uuid=uuid.uuid4(),
        tool='jira',
        username=getenv('JIRA_USER_EMAIL', ''),
        password='',
        token=getenv('JIRA_TOKEN', ''),
        login_url=getenv('JIRA_SERVER', ''),
        data_url='',
    )
    list_of_jira_projects = await read_projects(filter='tool="jira"')
    developer_registration = await read_developers()
    developers = [
        [
            developer['full_name'],
            developer['jira_name'],
            developer['jira_project_manager'],
        ]
        for developer in developer_registration
        if developer['jira_name']
    ]
    jira_items = await get_items_from_jira_projects(
        management_tool, list_of_jira_projects, developers
    )

    all_jira_items_saved = await save_tasks(list_of_jira_projects, jira_items)

    assert all_jira_items_saved
