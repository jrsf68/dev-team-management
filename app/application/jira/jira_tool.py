import uuid
from datetime import datetime, timezone

from jira import JIRA, JIRAError

from app.domain.entities import ManagementTools


async def get_items_from_jira_projects(
    management_tool: ManagementTools, list_of_jira_projects, developers
):
    jiraOptions = {'server': management_tool.login_url}
    jira = JIRA(
        options=jiraOptions,
        basic_auth=(management_tool.username, management_tool.token),
    )
    items = []
    for project in list_of_jira_projects:
        try:
            issues = jira.search_issues(f'project="{project["key"]}"')
        except JIRAError:
            continue
        list_of_jira_project_items = [
            {
                'uuid': str(uuid.uuid4()),
                'tool': 'jira',
                'developer_full_name': [
                    developer[0]
                    for developer in developers
                    if developer[1] == issue.fields.assignee.displayName  # type: ignore
                ][0],
                'developer_email': '',
                'developer_name': issue.fields.assignee.displayName,  # type: ignore
                'project': issue.fields.project.name,  # type: ignore
                'task_id': issue.key,  #  type: ignore
                'task': f'{issue.key} - {issue.fields.summary}',  # type: ignore
                'complement': issue.fields.status.name,  # type: ignore
                'completion_date': datetime.strptime(
                    issue.fields.updated, '%Y-%m-%dT%H:%M:%S.%f%z'  # type: ignore
                ).strftime('%Y-%m-%d %H:%M:%S'),
                'project_date': project['last_read_date'],
                'status': 'Complited',
            }
            for issue in issues
            if issue.fields.assignee is not None  # type: ignore
            and issue.fields.status.name != 'Tarefas pendentes'  # type: ignore
            and (
                datetime.strptime(
                    datetime.strptime(
                        issue.fields.updated, '%Y-%m-%dT%H:%M:%S.%f%z'  # type: ignore
                    ).strftime('%Y-%m-%d %H:%M:%S'),
                    '%Y-%m-%d %H:%M:%S',
                ).replace(tzinfo=timezone.utc)
            )
            > (
                datetime.strptime(
                    project['last_read_date'], '%Y-%m-%d %H:%M:%S'
                ).replace(tzinfo=timezone.utc)
            )
            and next(
                (
                    developer[1]
                    for developer in developers
                    if developer[1] == issue.fields.assignee.displayName  # type: ignore
                ),
                None,
            )
            is not None
        ]
        items.extend(list_of_jira_project_items)

    for project in list_of_jira_projects:
        try:
            issues = jira.search_issues(f'project={project["key"]}')
        except JIRAError:
            continue
        list_of_jira_project_items = [
            {
                'uuid': str(uuid.uuid4()),
                'tool': 'jira',
                'developer_full_name': [
                    developer[0]
                    for developer in developers
                    if (
                        developer[1] == issue.fields.reporter.displayName  # type: ignore
                        and developer[2] == 1
                    )
                ][0],
                'developer_email': '',
                'developer_name': issue.fields.reporter.displayName,  # type: ignore
                'project': issue.fields.project.name,  # type: ignore
                'task_id': issue.key,  #  type: ignore
                'task': f'{issue.key} - {issue.fields.summary}',  # type: ignore
                'complement': issue.fields.status.name,  # type: ignore
                'completion_date': datetime.strptime(
                    issue.fields.created, '%Y-%m-%dT%H:%M:%S.%f%z'  # type: ignore
                ).strftime('%Y-%m-%d %H:%M:%S'),
                'project_date': project['last_read_date'],
                'status': 'Created',
            }
            for issue in issues
            if issue.fields.reporter is not None  # type: ignore
            and issue.fields.status.name != 'Tarefas pendentes'  # type: ignore
            and (
                datetime.strptime(
                    datetime.strptime(
                        issue.fields.created, '%Y-%m-%dT%H:%M:%S.%f%z'  # type: ignore
                    ).strftime('%Y-%m-%d %H:%M:%S'),
                    '%Y-%m-%d %H:%M:%S',
                ).replace(tzinfo=timezone.utc)
            )
            > (
                datetime.strptime(
                    project['last_read_date'], '%Y-%m-%d %H:%M:%S'
                ).replace(tzinfo=timezone.utc)
            )
            and next(
                (
                    developer[1]
                    for developer in developers
                    if (
                        developer[1] == issue.fields.reporter.displayName  # type: ignore
                        and developer[2] == 1
                    )
                ),
                None,
            )
            is not None
        ]
        items.extend(list_of_jira_project_items)
    return items
