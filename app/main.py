"""Dev team management main program
"""

from fastapi import FastAPI

app = FastAPI()


@app.get('/status')
def read_root():
    """Check if the 'status' route is available."""
    return 'Ativo'


if __name__ == '__main__':
    read_root()
